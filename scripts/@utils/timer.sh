#!/bin/bash

# run termdown with a specified time then run
# terminal-notifier to alert termination of timer
source $HOME/.penv/venv/bin/activate
# termdown $1 && terminal-notifier \
#    -sound Glass \
#    -message "🙊 Get going! 🐒" \
#    -tile "Timer Elapsed" \
#    -subtitle "🙉 Ding! Ding! Ding! 🙈"

termdown $1 && osascript -e 'display alert "⏰" message "🙉 get going 🙈"'
deactivate
