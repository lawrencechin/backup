#!/bin/bash

# A Script to Clean up Photo Library Databases

# Paths
photo_path="/Volumes/Terry/Photos/Supreme.photoslibrary"
db_folder="$photo_path/database"
db_file="$db_folder/Photos.sqlite"
db_search_file="$db_folder/search/psi.sqlite"

# Functions
function drop_table {
    echo -e "\nDropping the ACHANGE table…"
    sqlite3 "$db_file" "DROP TABLE ACHANGE;"
    echo -e "\nDropping the ATRANSATION table…"
    sqlite3 "$db_file" "DROP TABLE ATRANSACTION"
}

function vacuum_tables {
    echo -e "\nVACUUMing databases…"
    sqlite3 "$db_file" "VACUUM;"
    sqlite3 "$db_search_file" "VACUUM;"
}

function backup_db_folder {
    echo -e "\nBacking up current database…"
    tar -vczf "$photo_path/../$( date +'%y-%m-%d_%H-%M-%S' )"-database_bck.tar.gz -C "$photo_path" database
}

if test -d "$db_folder" && test -f "$db_file" && test -f "$db_search_file"; then
    echo "The current count for the table ACHANGE is: $( sqlite3 $db_file 'SELECT COUNT(*) FROM ACHANGE;' )"
    echo "And table ATRANSACTION is: $( sqlite3 $db_file 'SELECT COUNT(*) FROM ATRANSACTION' )"
    while true; do
        echo -e "\nType 'b' to backup the current database folder \
            \nType 'd' to DROP the ACHANGE & ATRANSACTION tables \
            \nType 'v' to VACUUM the databases \
            \nType any other key to exit"
        read -p "> " ANS
        case $ANS in
            [bB]* )
                backup_db_folder
                ;;
            [dD]* ) echo -e "\nYou selected DROP"
                drop_table
                vacuum_tables
                ;;
            [vV]*) echo -e "\nYou chose VACUUM"
                vacuum_tables
                ;;
            * ) exit;;
        esac
    done
else
    echo "Cannot find the database folder or database"
fi
