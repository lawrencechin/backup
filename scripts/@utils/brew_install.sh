#!/bin/bash

for package in "$@"; do
    if [ -e "$( which $package )" ]; then
        brew reinstall -f "$( brew info --json $package | jq '.[0].bottle.stable.files.high_sierra.url' | sed s/\"//g )";
    else
        brew install -f "$( brew info --json $package | jq '.[0].bottle.stable.files.high_sierra.url' | sed s/\"//g )";
    fi
done

