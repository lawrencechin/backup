#!/bin/bash

nc="\033[0m"
cyan="\033[0;36m"
magenta="\033[0;35m"
green="\033[0;32m"
bold="\033[1m"
underline="\033[4m"

if which -s brew; then
    brew update;
    echo -e "${bold}Outdated${nc}"
    echo -e "${underline}Brew${nc} 🍺 :"
    echo -e "${cyan}•-----------------•${nc}"
    brew outdated
fi

if which -s port; then
    sudo port -v selfupdate
    echo -e "${underline}MacPort${nc} ⚓️ :"
    echo -e "${cyan}•-----------------•${nc}"
    port outdated
    echo -e "${cyan}\n• Inactive Ports •${nc}"
    echo -e "${cyan}•-----------------•${nc}"
    port installed inactive
fi

if test -x $(which npm); then
    echo -e "\n${underline}NPM${nc} 🎯 :"
    echo -e "${magenta}•-----------------•${nc}"
    npm -g outdated
fi

if test -d $HOME/.penv/venv; then
    echo -e "\n${underline}Python${nc} 🐍 :"
    echo -e "${green}•-----------------•${nc}"
    source $HOME/.penv/venv/bin/activate
    pip3 list --outdated
    deactivate
fi
