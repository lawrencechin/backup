#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

green="\033[0;32m"
red="\033[0;31m"
nc="\033[0m"

# Restore selected dotfiles
echo -e "${green}s${nc} > symlink dotfiles \n ${red}q${nc} > exit script"
while true; do
    read REPLY
    if [[ "$REPLY" = "s" ]]; then
        if test -d ~/.dev/@dotfiles; then
            ~/.dev/@dotfiles/dotfiles_link.sh
        else
            echo -e "${red}Dotfiles doesn't exit\! Grab the repo.${nc}"
        fi
        break
    elif [[ "$REPLY" = "q" ]]; then
        break
    else
        echo "Please select a valid option"
    fi
done
