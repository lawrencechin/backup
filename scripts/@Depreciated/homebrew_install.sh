#!/bin/bash
#Brew Shell Script - use after clean install

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# Homebrew taps
brew tap beeftornado/rmtree
brew tap homebrew/cask
brew tap homebrew/cask-drivers
brew tap homebrew/cask-versions
brew tap homebrew/core
# Homebrew main packages
while read bins; do
brew install "$bins"
done < "$DIR"/bin_list.txt
