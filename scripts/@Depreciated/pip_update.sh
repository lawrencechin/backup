#!/bin/bash

# List all packages installed with pip(3) and save to shell script
# Check if pip3 is installed first of all

if [[ "$(command -v pip3)" == "/usr/local/bin/pip3" ]]; then
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    echo -e "#!/bin/bash\n#Python package management - use after clean install \n" | tee "$DIR"/pip_install.sh
    echo "# Installed packages\n" | tee -a "$DIR"/pip_install.sh
    pip3 list --format=freeze | grep -Eio '([a-z_-]+)' | sed 's/^/pip3 install --upgrade /' | tee -a "$DIR"/pip_install.sh
    # make file executable
    chmod u+x "$DIR"/pip_install.sh
    echo "Python packages are saved… 📈"
else
    echo "Pip3 cannot be found. This probably means you haven't install Python3 yet so theres no point in installing any of these packages 🕵️‍♂️."
fi
