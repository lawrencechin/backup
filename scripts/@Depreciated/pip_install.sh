#!/bin/bash
#Python package management - use after clean install 

# Installed packages\n
pip3 install --upgrade astroid
pip3 install --upgrade click
pip3 install --upgrade greenlet
pip3 install --upgrade isort
pip3 install --upgrade lazy-object-proxy
pip3 install --upgrade mccabe
pip3 install --upgrade msgpack
pip3 install --upgrade pip
pip3 install --upgrade protobuf
pip3 install --upgrade pyfiglet
pip3 install --upgrade post
pip3 install --upgrade pylint
pip3 install --upgrade pynvim
pip3 install --upgrade python-dateutil
pip3 install --upgrade setuptools
pip3 install --upgrade six
pip3 install --upgrade termdown
pip3 install --upgrade toml
pip3 install --upgrade wheel
pip3 install --upgrade wrapt
