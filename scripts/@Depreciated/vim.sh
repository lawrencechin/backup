#!/bin/zsh
# Create directory and run 'dein' commands to set up the plugin environment for neo vim
DIR=${0%/*}
# Update - New installation directory
DEINDIR="$HOME/.cache/dein"

echo "Install Neo Vim plugin manager?\n y > proceed\n n > exit"

while true; do
    read REPLY
    if [[ "$REPLY" = "y" ]]; then
        mkdir -p $HOME/.config/nvim
        mkdir -p $HOME/.config/nvim/backup
        mkdir -p $HOME/.config/nvim/colors
        ln -s "$DIR/Vim Colour Schemes"/* "$HOME"/.config/nvim/colors/
        echo "It would be a jolly good idea to install dotfiles before proceeding. Do it! This will move your init.vim file into place. If you've already done it then ignore me."

        mkdir -p $DEINDIR
        curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > "$DEINDIR"/installer.sh
        sh "$DEINDIR"/installer.sh $DEINDIR
        break
    else 
        echo "Please select a valid option"
    fi
done
