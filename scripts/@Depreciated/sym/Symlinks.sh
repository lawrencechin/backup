#!/bin/sh
#check disk is mounted before continuing
echo "Ensure Explore is mounted before continuing"
while true
    read -p "Continue? (y/n) " -n 1 -r
do
	case $REPLY in
    [yY] ) #Logic Symlinks
        if [[ -d "/Volumes/Explore/Media/Music/@Logic Tracks" ]]; then
            sudo mkdir -p /Library/Application\ Support/Garageband/
            sudo mkdir -p /Library/Application\ Support/Garageband/Instrument\ Library
            sudo mkdir -p /Library/Application\ Support/Logic
            sudo ln -sf /Volumes/Explore/Media/Music/@Logic\ Tracks/LogicSamples/Sampler /Library/Application\ Support/Garageband/Instrument\ Library
            sudo ln -sf /Volumes/Explore/Media/Music/@Logic\ Tracks/LogicSamples/Audio/Apple\ Loops /Volumes/Explore/Media/Music/@Logic\ Tracks/LogicSamples/Audio/Impulse\ Responses /Library/Audio
            sudo ln -sf /Volumes/Explore/Media/Music/@Logic\ Tracks/LogicSamples/Channel\ Strip\ Settings /Volumes/Explore/Media/Music/@Logic\ Tracks/LogicSamples/Drummer /Volumes/Explore/Media/Music/@Logic\ Tracks/LogicSamples/EXS\ Factory\ Samples /Volumes/Explore/Media/Music/@Logic\ Tracks/LogicSamples/Logic\ Pro\ X\ Demosongs /Volumes/Explore/Media/Music/@Logic\ Tracks/LogicSamples/Patches /Volumes/Explore/Media/Music/@Logic\ Tracks/LogicSamples/Plug-In\ Settings /Volumes/Explore/Media/Music/@Logic\ Tracks/LogicSamples/Sampler\ Instruments /Volumes/Explore/Media/Music/@Logic\ Tracks/LogicSamples/Smart\ Controls /Volumes/Explore/Media/Music/@Logic\ Tracks/LogicSamples/Smart\ Map /Volumes/Explore/Media/Music/@Logic\ Tracks/LogicSamples/Ultrabeat\ Samples /Volumes/Explore/Media/Music/@Logic\ Tracks/LogicSamples/Alchemy\ Samples /Library/Application\ Support/Logic
            #iTunes Symlinks
            # mkdir -p /Users/$(whoami)/Music/iTunes/iTunes\ Media/
            # New version of iTunes (12.7) doesn't open if you symlink the media folder rather than the individual folders when the external drive isn't connected. Sucks for streaming podcasts
            # ln -sf /Volumes/Explore/Media/iTunes\ Media/Audiobooks /Volumes/Explore/Media/iTunes\ Media/Automatically\ Add\ to\ iTunes /Volumes/Explore/Media/iTunes\ Media/iTunes\ U /Volumes/Explore/Media/iTunes\ Media/Movies /Volumes/Explore/Media/iTunes\ Media/Music /Volumes/Explore/Media/iTunes\ Media/Podcasts /Volumes/Explore/Media/iTunes\ Media/Tones /Volumes/Explore/Media/iTunes\ Media/TV\ Shows /Users/$(whoami)/Music/iTunes/iTunes\ Media 
            break
        else
            echo "Explore has not been mounted, fix 🙈!"
        fi
        break;;
    [nN] ) break;;
    [*] ) echo "Please enter y or n" ;;
    esac
done		

