#!/bin/bash
# Fix Homebrew

red="\033[0;31m"
green="\033[0;32m"
nc="\033[0m"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
brewPath=/usr/local/Homebrew/Library/Homebrew/

if test -f "$brewPath"brew.sh; then
    touch "$DIR"brew_tmp.sh
    cat "$brewPath"brew.sh | sed -E 's/^(  HOMEBREW_MACOS_VERSION).+/\1\="10\.13\.6"/' | tee "$DIR"brew_tmp.sh
    mv "$DIR"brew_tmp.sh "$brewPath"brew.sh
    chmod a+x "$brewPath"brew.sh
    rm -f "$DIR"brew_tmp.sh
else
    echo "Homebrew not installed"
fi
