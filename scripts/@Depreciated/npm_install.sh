#!/bin/sh
#Node Module Shell Script - use after clean install
#And After installing homebrew/node

npm -g update eslint
npm -g update express
npm -g update gulp-cli
npm -g update hexo-cli
npm -g update jasmine-node
npm -g update node-gyp
npm -g update now
npm -g update tern
