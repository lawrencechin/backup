#!/bin/bash
#Backup brew files and update scripts
#Check if cellar exists
if [[ -d /usr/local/Cellar ]]; then
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	echo -e "#!/bin/bash\n#Brew Shell Script - use after clean install\n" | tee "$DIR"/homebrew_install.sh
    echo "# Homebrew taps" | tee -a "$DIR"/homebrew_install.sh
    brew tap | grep -v 'Updated' | grep -E '\w+\/' | sed 's/^/brew tap /' | tee -a "$DIR"/homebrew_install.sh
    echo "# Homebrew main packages" | tee -a "$DIR"/homebrew_install.sh
    echo "while read bins; do" | tee -a "$DIR"/homebrew_install.sh
    echo 'brew install "$bins"' | tee -a "$DIR"/homebrew_install.sh
    echo "done < bin_list.txt" | tee -a "$DIR"/homebrew_install.sh

    #brew leaves | sed 's/^/brew install /' | tee -a "$DIR"/homebrew_install.sh

    chmod u+x "$DIR"/homebrew_install.sh
    echo "Homebrew base packages updated 🍥"
else
	echo "Please install Homebrew before continuing 🧞‍♂️"
fi
