#!/bin/bash
#Backup brew cask files and update scripts
#Check if caskroom exists
if [[ -d /usr/local/Caskroom ]]; then
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	echo -e "#!/bin/bash\n#Brew Cask Shell Script - use after clean install\n" | tee "$DIR"/cask_install.sh
    brew list --cask | sed 's/^/brew cask install /' | tee -a "$DIR"/cask_install.sh

	chmod u+x "$DIR"/cask_install.sh
    echo "Casks updated in a slinky manner 🐊"
else
	echo "Couldn't find any casks, oops 🐍\!"
fi
