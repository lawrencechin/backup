#!/bin/bash
#Backup node modules and update scripts

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo -e "#!/bin/bash\n#Node Module Shell Script - use after clean install\n#And After installing homebrew/node and yarn\n" | tee "$DIR"/yarn_install.sh

yarn global list | grep -E ' - \S+' | sed -Ee 's/[[:space:]]+-/yarn global add/' -e 's/$/ --prefix \/usr\/local/' | tee -a "$DIR"/yarn_install.sh

# make file executable

chmod u+x "$DIR"/yarn_install.sh
echo "Yarn Modules are sexy and updated 🕷"
