#!/bin/bash
#Brew Cask Shell Script - use after clean install

if test -d /usr/local/Homebrew; then
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    while read bins; do
        brew cask install "$bins"
    done < "$DIR"/cask_list.txt
else
    echo "Homebrew not installed 💀"
fi
