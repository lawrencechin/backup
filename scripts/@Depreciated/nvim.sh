#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
nc="\033[0m"
red="\033[0;31m"
green="\033[0;32m"

echo -e  "Install Paq manager for Neovim?\n ${green}y${nc} > proceed${red}n${nc} n > exit"

while true; do
    read REPLY
    if [[ "$REPLY" = "y" ]]; then
        git clone --depth-1 https://github.com/savq/paq-nvim.git $HOME/.local/share/nvim/site/pack/paqs/start/paq-nvim
        echo "Paq installed. After Neovim has been installed and dotfiles moved into place, fire up nvim and install packages"
        break;;
    elif [[ "$REPLY" = "n" ]]; then
        break;;
    else 
        echo "Please select a valid option"
    fi
done
