#!/bin/bash
#Brew Shell Script - use after clean install

# Homebrew taps
brew tap beeftornado/rmtree
brew tap homebrew/cask
brew tap homebrew/cask-drivers
brew tap homebrew/cask-versions
brew tap homebrew/core
# Homebrew main packages
brew install coreutils
brew install eslint
brew install exercism
brew install findutils
brew install fish
brew install fortune
brew install git
brew install gnu-sed
brew install gptfdisk
brew install hugo
brew install imageoptim-cli
brew install jemalloc
brew install john-jumbo
brew install midnight-commander
brew install mit-scheme
brew install mps-youtube
brew install neofetch
brew install neovim
brew install p7zip
brew install pandoc
brew install python@2
brew install terminal-notifier
brew install tidy-html5
brew install tmux
brew install translate-shell
brew install unrar
brew install weechat
brew install wget
brew install yarn
brew install zsh
brew install zsh-syntax-highlighting
