#!/bin/zsh
#Install Prezto and set ZSH as the default shell
# Update Prezto
if [[ $1 ]]; then	
    # Built-in update didn't work from shell script
    # zprezto-update
    
    cd ~/.zprezto
    git stash # stash changes to dotfiles (they're now symlinks)
    git pull && git submodule update --init --recursive
    git stash clear # temporary - no need to keep them
    cd -
    echo "${bred}Configuration files may have changed!${nc} ${ured}Check changes between new files and backups/dotfiles.\nRun 'dotfiles' to relink config files${nc}"

    exit 1
fi

DIR=${0%/*}
echo "Install Prezto?\n y > continue \n n > return to main menu"
while true; do
    read REPLY

    if [[ "$REPLY" = "y" ]]; then
        echo "We're going to install Prezto and set ZSH as the default shell"
        echo "Clone Prezto repo"
        git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
        echo "Symlink the config files into home directory"

        setopt EXTENDED_GLOB

        for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do
            ln -sf "$rcfile" "${ZDOTDOT:-$HOME}/.${rcfile:t}"
        done

        echo "Set ZSH as default shell"

        chsh -s /bin/zsh

        echo "All done! It would be wise to setup you dotfiles and reload your shell"
        break
    elif [[ "$REPLY" = "n" ]]; then
        break
    else
        echo "Please select a valid option"
    fi
done
