#!/bin/bash
#Install Backup files to locations in ~/Library
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

me=$(whoami)
file_path="/Users/$me/.dev/@backup/other files/"
mode_switch="$file_path"/mode_switch
local_path="/Users/$me/.local/share"
yarn_bin="/Users/$me/.yarn/bin/"

function link_to_yarn_bin {
    # symlinks from applications
    ffmpeg="/Users/$me/Downloads/No Look Pass/@JDownloader 2/tools/mac/ffmpeg_10.10+/ffmpeg"
    ffprobe="/Users/$me/Downloads/No Look Pass/@JDownloader 2/tools/mac/ffmpeg_10.10+/ffprobe"
    jpegtran="/Users/$me/Applications/ImageOptim.app/Contents/Frameworks/ImageOptimGPL.framework/Versions/A/Resources/jpegtran"
    mpv="/Applications/mpv.app/Contents/MacOS/mpv"
    waifu2x="/Users/$me/Applications/waifu2x-mac-app.app/Contents/MacOS/waifu2x"
    port_app_arr=( "$ffmpeg" "$ffprobe" "$jpegtran" "$mpv" "$waifu2x" )
    brew_app_arr=( "$jpegtran" "$waifu2x" )
    # If using homebrew, ffmpeg and mpv will be installed as packages

    mkdir -p "$yarn_bin"
    if which -s brew; then
        for app_bin in "${brew_app_arr[@]}"; do
            if test -f "$app_bin"; then
                ln -sf "$app_bin" "$yarn_bin"
            fi
        done
    else
        for app_bin in "${port_app_arr[@]}"; do
            if test -f "$app_bin"; then
                ln -sf "$app_bin" "$yarn_bin"
            fi
        done
    fi

    ln -s "$file_path"dark-notify "$yarn_bin"
    ln -s "$file_path"silnite "$yarn_bin"
    #ln -s "$file_path"tccplus "$yarn_bin"
    ln -s "$file_path"wallpapper "$yarn_bin"
    ln -s "$file_path"wallpapper-exif "$yarn_bin"
}

function mode_switch_linker {
    mkdir -p "$local_path"
    cp -f "$mode_switch"/mode_switch.sh "$local_path"
    cp -f "$mode_switch"/wallpapers.db "$local_path"
}

function colour_pickers {
    for f in "$DIR/../../colorpickers/"*; do 
        cp -vr "$f" ~/Library/ColorPickers
    done
}

function colours {
    for f in "$DIR/../../colors/"*; do
        cp -vr "$f" ~/Library/Colors
    done
}

function system_sounds {
    for f in "$DIR/../../sounds/"*; do
        cp -vr "$f" ~/Library/Sounds
    done
}

while true
    echo "Install Colour Pickers? [y or n] > "
    read -r REPLY; do
    case "$REPLY" in
        [yY] ) colour_pickers ; break ;;
        * ) break ;;
    esac
done

while true
    echo "Install Colour Palettes? [y or n] > "
    read -r REPLY; do
    case "$REPLY" in
        [yY] ) colours ; break ;;
        * ) break ;;
    esac
done

while true
    echo "Install System Sounds? [y or n] > "
    read -r REPLY; do
    case "$REPLY" in
        [yY] ) system_sounds ; break ;;
        * ) break ;;
    esac
done

while true
    echo "Link bins to Yarn Global directory? [ y or n ] > "
    read -r REPLY; do
    case "$REPLY" in
        [yY] ) link_to_yarn_bin ; break ;;
        * ) break ;;
    esac
done

while true
    echo "Install mode switch to ~/.local/share? [ y or n ] > "
    read -r REPLY; do
    case "$REPLY" in
        [yY] ) mode_switch_linker ; break ;;
        * ) break ;;
    esac
done
