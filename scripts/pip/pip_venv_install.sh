#!/bin/bash
#Python package management - use after clean install 

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function install_pip_packages(){
    while read bins; do
        pip install --upgrade "$bins"
    done < "$DIR"/package_list.txt
}

function activate_install_deactivate(){
    source $HOME/.penv/venv/bin/activate
    install_pip_packages
    deactivate
}

if test -d $HOME/.penv/venv/bin; then
    activate_install_deactivate
elif test -f $(which virtualenv); then
    virtualenv $HOME/.penv/venv
    activate_install_deactivate
else
    echo " Virtualenv not found, install ports!"
fi
