#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

enable_plists () {
    while read items; do
        if [ "${items:0:1}" != "#" ] && [ "$items" ]; then
            echo "Enabling $items …"
            if [ -f /System/Library/$1/$items.plist ]; then
                if [ "$4" = "sudo" ]; then
                    "$4" launchctl enable "$2$items"
                    "$4" launchctl bootstrap "$2" /System/Library/"$1"/"$items.plist"
                else
                    launchctl enable "$2$items"
                    launchctl bootstrap "$2" /System/Library/"$1"/"$items.plist"
                fi
            else 
                echo "$items does not exit"
            fi
        fi
    done < "$DIR"/$3.txt
}

enable_plists "LaunchAgents" "gui/501/" "agents"
enable_plists "LaunchDaemons" "system/" "daemons" "sudo"
