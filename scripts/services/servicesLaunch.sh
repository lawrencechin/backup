#!/bin/bash

# Source: https://gist.github.com/b0gdanw/40d000342dd1ba4d892ad0bdf03ae6ea

#Credit: Original idea and script disable.sh by pwnsdx https://gist.github.com/pwnsdx/d87b034c4c0210b988040ad2f85a68d3
#*****Work in progress*****
#Disabling unwanted services on macOS 11 Big Sur
#Modifications written in /private/var/db/com.apple.xpc.launchd/ disabled.plist, disabled.501.plist & disabled.205.plist
#Disabling SIP might not be required, still testing.

# Thread on MacRumors leading to this script: https://forums.macrumors.com/threads/update-to-10-15-5.2239185/

# IMPORTANT: Don't forget to logout from your Apple ID in the settings before running it!
# IMPORTANT: You will need to run this script from Recovery. In fact, macOS Catalina brings read-only filesystem which prevent this script from working from the main OS.
# This script needs to be run from the volume you wish to use.
# E.g. run it like this: cd /Volumes/Macintosh\ HD && sh /Volumes/Macintosh\ HD/Users/sabri/Desktop/disable.sh
# WARNING: It might disable things that you may not like. Please double check the services in the TODISABLE vars.

# Get active services: launchctl list | grep -v "\-\t0"
# Find a service: grep -lR [service] /System/Library/Launch* /Library/Launch* ~/Library/LaunchAgents

# Mounting system as writable
# sudo mount -wu /

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "🐯 > The following script will either ENABLE or DISABLE macOS services. Press 'd' to DISABLE, 'e' to ENABLE or any other key to quit"

while true; do
    read REPLY
    if [[ "$REPLY" = "d" ]]; then
        if test -x "$DIR"/disable.sh; then
            echo "🐯 > Disabling services now, you should restart after it has finished"
            "$DIR"/disable.sh
        else
            echo "🐯 > It seems the script doesn't exist, something has gone awry"
        fi
        break
    elif [[ "$REPLY" = "e" ]]; then
        if test -x "$DIR"/enable.sh; then
            echo "🐯 > Enabling services now, you should restart after it has finished"
            "$DIR"/enable.sh
        else
            echo "🐯 > It seems the script doesn't exist, something has gone awry"
        fi
        break
    else
        break
    fi
done
