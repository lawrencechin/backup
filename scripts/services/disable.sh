#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

disable_plists () {
    while read items; do
        if [ "${items:0:1}" != "#" ] && [ "$items" ]; then
            echo "Disabling $items …"
            if [ -f /System/Library/$1/$items.plist ]; then
                if [ "$4" = "sudo" ]; then
                    "$4" launchctl bootout "$2$items"
                    "$4" launchctl disable "$2$items" 
                else
                    launchctl bootout "$2$items"
                    launchctl disable "$2$items"
                fi
            else 
                echo "$items does not exit"
            fi
        fi
    done < "$DIR"/$3.txt
}

disable_plists "LaunchAgents" "gui/501/" "agents"
disable_plists "LaunchDaemons" "system/" "daemons" "sudo"
