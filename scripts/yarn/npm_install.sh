#!/bin/bash
# Install Global NPM Modules  - use after clean install

if test -x $(which npm); then
    echo "Setting global bin folder to: .yarn/bin"
    npm config set prefix "~/.yarn"
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    while read bins; do
        npm -g install "$bins"
    done < "$DIR"/package_list.txt
    echo "Cleaning cache with: npm cache clean --force"
    npm cache clean --force
    echo "Remember to add global dir to PATH"
    echo "For fish use: fish_add_path (npm prefix -g)"
else
    echo "Npm not installed, que desastre 🤮"
fi
