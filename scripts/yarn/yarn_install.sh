#!/bin/bash
# Install Global Yarn Modules  - use after clean install

if test -d $HOME/.yarn/bin; then
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    while read bins; do
        yarn global add "$bins"
    done < "$DIR"/package_list.txt
    echo "Remember to add global dir to PATH"
    echo "For fish use: fish_add_path (yarn global bin)"
else
    echo "Yarn not installed, que desastre 🤮"
fi
