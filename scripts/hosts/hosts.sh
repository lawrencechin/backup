#!/bin/bash

nc="\033[0m"
magenta="\033[0;35m"
cyan="\033[0;36m"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo -e "\n${magenta}1${nc}. Update and install hosts"
echo -e "${cyan}2${nc}. Restore original hosts file"
echo -e "${magenta}3${nc}. Compose DNSCloak hosts.txt"
echo -e "${cyan}q${nc}. Exit\n"

read -p ": " -n 1 REPLY
if test "$REPLY" = "1";then
    sudo bash "$DIR"/hosts_backup.sh
    echo -e "Hosts updated.\n"
elif test "$REPLY" = "2"; then
    sudo bash "$DIR"/hosts_restore.sh
    echo -e "Hosts restored.\n"
elif test "$REPLY" = "3"; then
    "$DIR"/hosts_plaintxt.sh
    echo -e "Generated plain text hosts\n"
else
    exit
fi

