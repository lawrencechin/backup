#!/bin/bash

#Set the current directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

curl "https://raw.githubusercontent.com/jawz101/MobileAdTrackers/master/hosts" > "$DIR"/h1

# Seems far too heavy
# curl "https://raw.githubusercontent.com/jerryn70/GoodbyeAds/master/Hosts/GoodbyeAds.txt" >> "$DIR"/h1

# https://github.com/BlackJack8/iOSAdblockList
curl "https://raw.githubusercontent.com/BlackJack8/iOSAdblockList/master/iPv4Hosts.txt" >> "$DIR"/h1

# Fix google fonts and BBC being completely blocked - a bit of a 
# sledgehammer approach in regards to BBC but I listen to a lot of rado
grep -v -e "0.0.0.0 googleadapis" -e bbc "$DIR"/h1 > "$DIR"/h2

# Place file in iCloud Drive for iOS access
grep "0.0.0.0 " "$DIR"/h2 | sed -E 's/0\.0\.0\.0\ //' > "$ICDOCS/Documents/@Dev/hosts.txt"

# Remove temp files

rm "$DIR"/h1 "$DIR"/h2
