#!/bin/bash

#Set the current directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Backup fresh hosts file if not already done
if test -e !/private/etc/hosts.bak; then
    echo "Backing up hosts to hosts.bak"
    sudo cp /private/etc/hosts /private/etc/hosts.bak
fi

# Get new hosts file
# https://github.com/StevenBlack/hosts
sudo curl "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts" > "$DIR"/hosts_1
grep -v "0.0.0.0 googleadapis" "$DIR"/hosts_1 > "$DIR"/hosts_2
echo -e "\n# Next list\n" >> "$DIR"/hosts_2

# https://github.com/BlackJack8/iOSAdblockList
curl "https://raw.githubusercontent.com/BlackJack8/iOSAdblockList/master/iPv4Hosts.txt" >> "$DIR"hosts_2

# Add custom entries
echo -e "\n# Custom Entries\n" >> "$DIR"/hosts_2
cat "$DIR"/custom_entries.txt >> "$DIR"/hosts_2

# Move "$DIR"hosts_2 to /private/etc/hosts

sudo cat "$DIR"/hosts_2 > /private/etc/hosts

# Remove temp files
sudo rm "$DIR"/hosts_1 "$DIR"/hosts_2

# Flush DNS - macOS only at the moment
echo "Flushing DNS cache"
sudo dscacheutil -flushcache
sudo killall -HUP mDNSResponder
