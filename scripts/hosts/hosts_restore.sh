#!/bin/bash
# Set the current directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Restore hosts.bak
sudo mv /private/etc/hosts.bak /private/etc/hosts

# Flush DNS cache - macOS only, use uname and switches for cross-plat
sudo dscacheutil -flushcache;sudo killall -HUP mDNSResponder
