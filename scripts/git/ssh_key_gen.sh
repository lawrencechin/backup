#!/bin/bash
# SSH Key generations

red="\033[0;31m"
green="\033[0;32m"
nc="\033[0m"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo -e "Configure ${green}ssh key${nc}. Type your email address. Type ${red}q${nc} to quit"

while true; do
    IFS=','
    read REPLY
    if test "$REPLY" = "q"; then
        break
    else
        ssh-keygen -t ed25519 -C "$REPLY"
        echo "Add your public key to your remote git account"
        echo "Run this command: pbcopy < ~/.ssh/id_ed25519.pub"
        echo "Paste the key into the correct location for ${green}GitLab${nc} or ${green}GitHub${nc} etc…"
        break
    fi
done
