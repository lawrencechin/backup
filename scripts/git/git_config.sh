#!/bin/bash
# Git Config Setup

red="\033[0;31m"
green="\033[0;32m"
nc="\033[0m"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo -e "Configure ${green}.gitconfig${nc}. Type your name and email address separated with a comma. Spaces are allowed in name. Type ${red}q${nc} to quit"

while true; do
    IFS=','
    read REPLY1 REPLY2 
    if test "$REPLY1" = "q" -o "$REPLY1" = "Q"; then
        break
    else
        git config --global --unset user.name
        git config --global --unset user.email
        git config --global user.name "$REPLY1"
        git config --global user.email "$REPLY2"
        git config --global color.ui true
        break
    fi
done
