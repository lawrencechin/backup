#!/bin/bash

# Current Dir
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Colors
black="\033[0;30m"
red="\033[0;31m"
green="\033[0;32m"
orange="\033[0;33m"
blue="\033[0;34m"
magenta="\033[0;35m"
light_magenta="\033[1;35m"
cyan="\033[0;36m"
light_gray="\033[0;37m"
dark_gray="\033[1;30m"
yellow="\033[1;33m"
white="\033[1;37m"
nc="\033[0m"

# Arrays
main_options=( 
    "Install Macports"
    "Install Homebrew"
    "Link Dotfiles"
    "Git Config"
    "SSH Key Gen")

secondary_options=(
    "Install Ports"
    "View Application List"
    "Install Homebrew Bottles"
	"Install Homebrew Casks"
	"Install NPM Packages"
	"Copy Files to ~/Library"
    "Install Python packages"
    "Hosts File" 
    "MacOS services" )

echo -e "\n${green}Welcome!${nc} Please select an ${green}option${nc}.\n"
echo -e "Use the following command to switch shells ${magenta}chsh -s path/to/shell${nc}"
echo -e "Custom Shells need to be added to ${magenta}/etc/shells${nc}\n"

options(){
    incrementer=1
    divider="${cyan}--------------------------${nc}"
    IFS=""
    for i in "${main_options[@]}"; do

        if ! (( incrementer % 2 )); then
             echo -e "${blue}$incrementer.${nc} ${magenta}$i${nc}"
        else
             echo -e "${blue}$incrementer.${nc} $i"
        fi
        
        incrementer=$(( incrementer + 1 ))
    done
     echo -e "$divider"
    for i in "${secondary_options[@]}"; do
        if ! (( incrementer % 2 )); then
             echo -e "${blue}$incrementer.${nc} ${light_magenta}$i${nc}"
        else
             echo -e "${blue}$incrementer.${nc} $i"
        fi
        
        incrementer=$(( incrementer + 1 ))
    done
    echo -e "$divider"
	echo -e "Type ${green}p${nc} to ${green}print${nc} options"
	echo -e "Type ${red}q${nc} to ${red}exit${nc} script"
}

install_macports() {
    echo -e "Installing command line tools. Should the ${magenta}gcc${nc} command run then clt is installed already."
    gcc
    echo -e "Visit ${magenta}https://www.macports.org/install.php${nc} and download the package installer"
}

view_apps() {
    cat "$DIR"/homebrew/cask_list.txt
}

options

while true
    read -rp "> " -n 2 REPLY; do
        echo -e "${cyan} ->->- ${nc}"
        case $REPLY in
        1 ) install_macports ;;
        2 ) "$DIR"/homebrew/homebrew.sh ;;
        3 ) "$DIR"/dotfiles/restore_dotfiles.sh ;;
        4 ) "$DIR"/git/git_config.sh ;;
        5 ) "$DIR"/git/ssh_key_gen.sh ;;
        6 ) "$DIR"/homebrew/macports_install.sh ;;
        7 ) view_apps ;;
        8 ) "$DIR"/homebrew/homebrew_install.sh ;;
        9 ) "$DIR"/homebrew/cask_install.sh ;;
        10 ) "$DIR"/yarn/npm_install.sh ;;
        11 ) "$DIR"/install_files/install_files.sh ;;	
        12 ) "$DIR"/pip/pip_venv_install.sh ;;
        13 ) "$DIR"/hosts/hosts.sh ;;
        14 ) "$DIR"/services/servicesLaunch.sh ;;
        [pP] ) options ;;	
        [qQ] ) break ;;
        * )  echo -e "Select an ${green}option${nc}, ${green}p${nc} to list ${green}options${nc} or ${red}q${nc} to ${red}exit${nc}" ;;
        esac
done			

echo -e "\n${red}Bye!${nc}"
