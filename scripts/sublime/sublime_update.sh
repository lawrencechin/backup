#!/bin/bash
#Backup Sublime Text user files(profile and packages) 
#Check if sublime text files exist first
    if [[ -d $HOME/Library/Application\ Support/Sublime\ Text\ 3/ ]]; then
        DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

        cat $HOME/Library/Application\ Support/Sublime\ Text\ 3/Packages/User/Package\ Control.sublime-settings | tee "$DIR"/sublime_packages.txt

        cat $HOME/Library/Application\ Support/Sublime\ Text\ 3/Packages/User/Preferences.sublime-settings | tee "$DIR"/sublime_prefs.txt
else
    echo "Sublime Text is not installed 🐴."
fi



