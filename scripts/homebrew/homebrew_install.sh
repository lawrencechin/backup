#!/bin/bash
#Homebrew Install Script - use after clean install

if which -s brew ; then
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    while read -r bins; do
        brew install "$bins" < /dev/null
    done < "$DIR"/bin_list.txt
    while read -r bons; do
        brew install "$bons" < /dev/null
    done < "$DIR"/bin_list_brew.txt
    echo "Ensure that /usr/local/ is in your PATH"
else
    echo "Homebrew not installed, boo 🙅🏻"
fi