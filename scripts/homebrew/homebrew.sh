#!/bin/bash
# Install Homebrew

red="\033[0;31m"
green="\033[0;32m"
nc="\033[0m"

echo -e "Install homebrew?\n ${green}y${nc} > proceed\n ${red}n${nc} > exit"

while true; do
    read REPLY
    if [[ "$REPLY" = "y" ]]; then
        /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
        break
    elif [[ "$REPLY" = "n" ]]; then
        break
    else
        echo "Please select a valid option"
    fi
done
