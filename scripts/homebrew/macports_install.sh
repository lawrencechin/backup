#!/bin/bash
#Macports Install Script - use after clean install

if test -d /opt/local; then
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    while read bins; do
        sudo port install "$bins"
    done < "$DIR"/bin_list.txt
    echo "Python may have been installed as a dependency. If it hasn't, install it. If it has, install the relevant version of pip & virtualenv and setup virtualenv at ~/.penv/venv"
    echo "Ensure that both /opt/local & /usr/local/ are in your PATH"
else
    echo "Macports not installed, boo 🙅🏻"
fi
