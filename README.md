# @Backup

![image](image.jpg)

**@Backup** has two main functions: restore files and packages to a fresh install of **macOS** and create lists of currently installed packages. The latter pulls in from [homebrew](https://brew.sh), [pip](https://pypi.org/project/pip/) and [yarn](https://yarnpkg.com/lang/en/).


