# Waifu 2x Mac

Should you ever wish to install this again, the instructions are as follows:

## Homebrew (Experimental, CLI only)

If you want to install with homebrew: `brew install imxieyi/waifu2x/waifu2x`

It downloads pre-built binary from releases. Xcode is not needed.

Good. And here's a [link](https://github.com/imxieyi/waifu2x-mac) to the repo.
