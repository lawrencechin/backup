#!/bin/bash
# Switch wallpaper and kitty theme on dark/light mode switch
# using dark-notify

# variables
dir=~/.local/share/
db="$dir/wallpapers.db"
ap_script_fragment="_wallpaper_random.applescript"
kitty_path="$HOME/Applications/kitty.app/Contents/MacOS/kitty"
kitty_config=~/.config/kitty
sql3="/usr/bin/sqlite3"

# detect current active mode
active_mode(){
    mode=$( "$sql3" "$db" "SELECT type FROM Mode WHERE id = ( SELECT Themes.mode_id FROM Themes, Selected WHERE Themes.id = Selected.theme_id )" )
    if test "$mode" = "$1"; then
        return 1
    else
        return 0
    fi
}

# update sqlite database on mode change
update_db() {
    "$sql3" "$db" \
        "UPDATE Selected SET theme_id = \
            ( SELECT Themes.id FROM Themes WHERE Themes.mode_id = \
                ( SELECT id FROM Mode WHERE Themes.active = 1 AND Mode.type = \"$1\" ) \
            ORDER BY Random() LIMIT 1 ) \
        WHERE id = 1; \
        UPDATE Selected SET wallpaper_id = \
            ( SELECT Theme_Wallpaper_Link.wallpaper_id FROM Theme_Wallpaper_Link WHERE Theme_Wallpaper_Link.theme_id = Selected.theme_id ORDER BY Random() LIMIT 1) \
        WHERE id = 1;
        UPDATE Selected SET type_id = \
            ( SELECT Type.id FROM Type ORDER by Random() LIMIT 1) \
        WHERE id = 1;"
}

# set kitty theme using db
set_kitty_theme() {
    theme_name=$("$sql3" "$db" 'SELECT Themes.name FROM Themes, Selected WHERE Themes.id = Selected.theme_id;' )
    "$kitty_path" +kitten themes --reload-in=all "$theme_name"
}

# kitten themes currently broken, alternative function
set_kitten_theme() {
    theme_name=$("$sql3" "$db" 'SELECT Themes.name FROM Themes, Selected WHERE Themes.id = Selected.theme_id;' )
    # dump theme into current-theme.conf
    cat "$kitty_config"/themes/"$theme_name".conf > "$kitty_config"/current-theme.conf
    # reload kitty conf
    "$kitty_path" +runpy "from kitty.utils import *; reload_conf_in_all_kitties()"
}

# this will only work when terminal is running
set_terminal_theme() {
    theme_name=$("$sql3" "$db" 'Select Themes.name FROM Themes, Selected WHERE Themes.id = Selected.theme_id;' )
    type_name=$("$sql3" "$db" 'Select Type.font_name FROM Type, Selected WHERE Type.id = Selected.type_id;' )
    type_size=$("$sql3" "$db" 'Select Type.font_size FROM Type, Selected WHERE Type.id = Selected.type_id;' )
    echo "$theme_name | $type_name ($type_size)"
    osascript <<END
        tell application "Terminal"
            repeat with w in windows
                repeat with t in tabs of w
                    set current settings of t to settings set "$theme_name"
                    set font name of current settings of t to "$type_name"
                    set font size of current settings of t to "$type_size"
                end repeat
            end repeat
        end tell
END
}

# set wallpaper using applescript
set_wallpaper() {
    osascript "${dir}${1}${ap_script_fragment}"
}

# set wallpaper using shortcuts
set_wallpaper_shortcuts(){
    shortcuts run "Set Wallpaper iMac"
}

# functions receive "light" or "dark" from dark_notify
if active_mode "$@"; then
    update_db "$@"
    #set_kitten_theme
    set_terminal_theme
    set_wallpaper_shortcuts "$@"
fi
