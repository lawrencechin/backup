tell application "Finder"
	set wallpaper to false
	repeat until wallpaper
		set random_image to some file of folder ".local:share:wallpapers:light" of home as alias
		set image_name to name of random_image
		if image_name starts with "nm_" or image_name starts with "po_" then
			set wallpaper to false
		else
			set wallpaper to true
		end if
	end repeat
	set desktop picture to random_image
end tell

